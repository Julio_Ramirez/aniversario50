$('.parallax-window').parallax({
    imageSrc: 'Fondos/01.jpg',
    androidFix: true,
    naturalHeight: 768,
});

$('.parallax-window-3').parallax({
  imageSrc: 'Fondos/BG_rojo.jpg',
  androidFix: true,
  naturalHeight: 768,
  positionX: 'center',
  positionY: 'bottom',
});

$('.parallax-window-2').parallax({
    imageSrc: 'Fondos/02.jpg',
    androidFix: true,
    naturalHeight: 768,
    positionX: 'center',
    positionY: 'bottom',
});


$(document).ready(function() {    
  
  //Animacion de Scroll
  $('a[href^="#"]').click(function() {
    var destino = $(this.hash);
    if (destino.length == 0) {
      destino = $('a[name="' + this.hash.substr(1) + '"]');
    }
    if (destino.length == 0) {
      destino = $('html');
    }
    $('html, body').animate({ scrollTop: destino.offset().top -50 }, 500);
    return false;
  });

  // Selector
  var menu = document.querySelector('.hamburger');

  // Funcion
  function toggleMenu(event) {
    this.classList.toggle('is-active');
    document.querySelector( ".menuppal" ).classList.toggle("is_active");
    event.preventDefault();
  }

  // Evento
  menu.addEventListener('click', toggleMenu, false);

  //Esconde el menu
  $('li > a').click(function(){
    $('.hamburger').toggleClass('is-active');
    document.querySelector( ".menuppal" ).classList.toggle("is_active");
  });

});



